<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
  
// include database and object files
include_once 'config/connection.php';
include_once 'objects/student.php';
  
// instantiate database and student object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$student = new Student($db);

if($_SERVER['REQUEST_METHOD'] == "GET"){
	if(isset($_GET['id'])){
		$student->readOne($_GET['id']);
		echo json_encode($student);
	}else{
		$stmt = $student->read();
		$students_arr=array();
		$students_arr["records"]=array();
	  
		if ($stmt) {
			while ($row = mysqli_fetch_row($stmt)) {
	  
			$student_item=array(
				"id" => $row[0],
				"name" => $row[1],
				"number" => $row[2],
				"age" => $row[3]
			);
	  
			array_push($students_arr["records"], $student_item);
			}
		}
	  
		// set response code - 200 OK
		http_response_code(200);
	  
		// show students data in json format
		echo json_encode($students_arr);
	}
}else{
	if($_SERVER['REQUEST_METHOD'] == "POST"){
// get posted data
$data = json_decode(file_get_contents("php://input"));

// make sure data is not empty
if(
    !empty($data->student_name) &&
    !empty($data->student_number) &&
    !empty($data->student_age)
){
  
    // set student property values
    $student->name = $data->student_name;
    $student->number = $data->student_number;
    $student->age = $data->student_age;
  
    // create the student
    if($student->create()){
  
        // set response code - 201 created
        http_response_code(201);
  
        // tell the user
        echo json_encode(array("message" => "student was created."));
    }
  
    // if unable to create the student, tell the user
    else{
  
        // set response code - 503 service unavailable
        http_response_code(503);
  
        // tell the user
        echo json_encode(array("message" => "Unable to create student."));
    }
}
  
// tell the user data is incomplete
else{
  
    // set response code - 400 bad request
    http_response_code(400);
  
    // tell the user
    echo json_encode(array("message" => "Unable to create student. Data is incomplete."));
}	}
}

?>