<?php

class Student{
  
    // database connection and table name
    private $conn;
    private $table_name = "student";
  
    // object properties
    public $id;
    public $number;
    public $name;
    public $age;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
	
	// read students
	function read(){
  
		// select all query
		$query = "SELECT
					id, student_name, student_number, student_age
				FROM
					" . $this->table_name;
	  
		// prepare query statement
		$stmt = $this->conn->query($query);
	  
		// execute query
		//$result = $stmt->execute();
	  
		return $stmt;
	}
	
	// create student
	function create(){
  
    // query to insert record
    $query = "INSERT INTO
                " . $this->table_name . "
            SET
                student_name=?, student_number=?, student_age=?";
  
    // prepare query
    $stmt = $this->conn->prepare($query);
  
    // sanitize
    $this->name=htmlspecialchars(strip_tags($this->name));
    $this->number=htmlspecialchars(strip_tags($this->number));
    $this->age=htmlspecialchars(strip_tags($this->age));
  
      // bind values
	$stmt->bind_param("ssi", $this->name, $this->number, $this->age);
  
    // execute query
    if($stmt->execute()){
        return true;
    }
  
    return false;
      
	}
	
	// used when filling up the update student form
	function readOne($id){
  
    // query to read single record
    $query = "SELECT
					student_name, student_number, student_age
				FROM
					" . $this->table_name . "
            WHERE
                id = ?
            LIMIT
                0,1";
  
    // prepare query statement
    $stmt = $this->conn->prepare( $query );
  
	$this->id = $id;
    // bind id of student to be updated
    $stmt->bind_param("s", $this->id);
  
    // execute query
    $stmt->execute();

    // get retrieved row
	$stmt->bind_result($name, $number, $age);
	$stmt->fetch();
    // set values to object properties
    $this->name = $name;
    $this->number = $number;
    $this->age = $age;
	}
	
	// update the student
	function update($id){
  
		// update query
		$query = "UPDATE
					" . $this->table_name . "
				SET
					student_name = ?,
					student_number = ?,
					student_age = ?
				WHERE
					id = ?";
	  
		// prepare query statement
		$stmt = $this->conn->prepare($query);
	  
		// sanitize
		$this->name=htmlspecialchars(strip_tags($this->name));
		$this->number=htmlspecialchars(strip_tags($this->number));
		$this->age=htmlspecialchars(strip_tags($this->age));
		$this->id=htmlspecialchars(strip_tags($id));
	  
		// bind new values
		$stmt->bind_param('ssii', $this->name, $this->number, $this->age, $this->id);
	  
		// execute the query
		if($stmt->execute()){
			return true;
		}
	  
		return false;
	}
	
	// delete the product
	function delete($id){
  
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
	  
		// prepare query
		$stmt = $this->conn->prepare($query);
	  
		// sanitize
		$this->id=htmlspecialchars(strip_tags($id));
	  
		// bind id of record to delete
		$stmt->bind_param('s', $this->id);
	  
		// execute query
		if($stmt->execute()){
			return true;
		}
	  
		return false;
	}
}
?>